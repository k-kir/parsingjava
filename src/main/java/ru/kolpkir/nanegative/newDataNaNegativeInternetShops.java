package ru.kolpkir.nanegative;

import ru.kolpkir.ParseLib.ParserWorker;
import ru.kolpkir.nanegative.model.internetShop;

import java.util.ArrayList;

public class newDataNaNegativeInternetShops implements ParserWorker.OnNewDataHandler<ArrayList<internetShop>> {
    ArrayList<internetShop> shops;

    public void OnNewData(Object sender, ArrayList<internetShop> args) {
        shops.addAll(args);
    }

    public newDataNaNegativeInternetShops(ArrayList<internetShop> shops) {
        this.shops = shops;
    }
}
