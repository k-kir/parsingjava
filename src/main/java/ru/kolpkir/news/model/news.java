package ru.kolpkir.news.model;

public class news {
    public String heading;
    public String text;
    public String link;

    public news(String link) {
        this.link = link;
    }

    public news(String heading, String text) {
        this.heading = heading;
        this.text = text;
    }
}
