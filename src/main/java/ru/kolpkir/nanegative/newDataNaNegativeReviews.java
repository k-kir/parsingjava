package ru.kolpkir.nanegative;

import ru.kolpkir.ParseLib.ParserWorker;
import ru.kolpkir.nanegative.model.review;

import java.util.ArrayList;

public class newDataNaNegativeReviews implements ParserWorker.OnNewDataHandler<ArrayList<review>> {
    ArrayList<review> reviews;

    public void OnNewData(Object sender, ArrayList<review> args) {
        reviews.addAll(args);
    }

    public newDataNaNegativeReviews(ArrayList<review> reviews) {
        this.reviews = reviews;
    }
}
