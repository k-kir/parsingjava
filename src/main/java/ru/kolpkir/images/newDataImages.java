package ru.kolpkir.images;

import ru.kolpkir.ParseLib.ParserWorker;

import java.util.ArrayList;

public class newDataImages implements ParserWorker.OnNewDataHandler<ArrayList<String>> {
    ArrayList<String> links;

    public void OnNewData(Object sender, ArrayList<String> args) {
        links.addAll(args);
    }

    public newDataImages(ArrayList<String> links) {
        this.links = links;
    }
}
