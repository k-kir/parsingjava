package ru.kolpkir.leroymerlin.model;

public class review {
    double rating;
    String author;
    String datePublished;
    String comment;

    public double getRating() {
        return rating;
    }

    public String getAuthor() {
        return author;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public String getComment() {
        return comment;
    }

    public review() {
    }

    public review(double rating, String author, String datePublished, String comment) {
        this.rating = rating;
        this.author = author;
        this.datePublished = datePublished;
        this.comment = comment;
    }
}
