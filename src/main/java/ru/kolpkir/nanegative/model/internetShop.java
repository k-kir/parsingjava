package ru.kolpkir.nanegative.model;

import java.util.ArrayList;

public class internetShop {
    String name;
    String link;
    byte averageRating;
    int countOfReviews;
    public ArrayList<review> reviews;

    public internetShop(String name, String link, byte averageRating, int countOfReviews) {
        reviews = new ArrayList<review>();
        this.name = name;
        this.link = link;
        this.averageRating = averageRating;
        this.countOfReviews = countOfReviews;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public byte getAverageRating() {
        return averageRating;
    }

    public int getCountOfReviews() {
        return countOfReviews;
    }
}
