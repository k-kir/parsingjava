package ru.kolpkir.news;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;
import ru.kolpkir.news.model.news;

import java.util.ArrayList;

public class bbcParser implements Parser<ArrayList<news>> {
    int count;

    public ArrayList<news> Parse(Document document) {
        ArrayList<news> list = new ArrayList<news>();
        Elements newss = document.getElementsByClass("nw-o-keyline");
        int i = 0;
        for (var ns : newss) {
            if (i > count)
                break;
            try {
                String link = ns.getElementsByClass("gs-o-faux-block-link__overlay-link").get(0).absUrl("href");
                list.add(new news(link));
                if (!ParseNews(list.get(i))) {
                    list.remove(i--);
                }
                i++;
            } catch (Exception ex) {

            }
        }
        return list;
    }

    private boolean ParseNews(news ns) {
        try {
            Document nsDoc = Jsoup.connect(ns.link).get();
            ns.heading = nsDoc.getElementsByTag("h1").get(0).text();
            ns.text = "";
            for (var textBlock : nsDoc.getElementsByAttributeValue("data-component", "text-block")) {
                ns.text += textBlock.text();
            }
            if (ns.text.equals(""))
                return false;
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public bbcParser(int count) {
        this.count = count;
    }
}
