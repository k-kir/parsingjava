package ru.kolpkir.news;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;
import ru.kolpkir.news.model.news;

import java.util.ArrayList;

public class tassParser implements Parser<ArrayList<news>> {
    int count;

    public ArrayList<news> Parse(Document document) {
        ArrayList<news> list = new ArrayList<news>();
        Elements newss = document.getElementsByClass("news-list__item");
        int i = 0;
        for (var ns : newss) {
            if (i > count)
                break;
            try {
                String link = ns.getElementsByClass("news-preview_default").get(0).absUrl("href");
                list.add(new news(link));
                if (!ParseNews(list.get(i))) {
                    list.remove(i--);
                }
                i++;
            } catch (Exception ex) {

            }
        }
        return list;
    }

    private boolean ParseNews(news ns) {
        try {
            Document nsDoc = Jsoup.connect(ns.link).get();
            ns.heading = nsDoc.getElementsByClass("news-header__title").get(0).text();
            ns.text = nsDoc.getElementsByClass("text-content").text();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public tassParser(int count) {
        this.count = count;
    }
}
