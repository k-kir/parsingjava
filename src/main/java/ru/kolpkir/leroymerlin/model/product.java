package ru.kolpkir.leroymerlin.model;

import java.util.ArrayList;

public class product {
    String price;
    String name;
    String link;
    public ArrayList<review> reviews;

    public product() {
        reviews = new ArrayList<>();
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public product(String price, String name, String link) {
        reviews = new ArrayList<>();
        this.price = price;
        this.name = name;
        this.link = link;
    }
}
