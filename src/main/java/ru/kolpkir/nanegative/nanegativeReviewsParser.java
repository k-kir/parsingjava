package ru.kolpkir.nanegative;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;
import ru.kolpkir.nanegative.model.review;

import java.util.ArrayList;

public class nanegativeReviewsParser implements Parser<ArrayList<review>> {
    public ArrayList<review> Parse(Document document) {
        ArrayList<review> list = new ArrayList<review>();
        Elements reviews = document.getElementsByClass("reviewers-box");
        for (var rev : reviews) {
            byte rating = Byte.parseByte(rev.getElementsByAttributeValue("itemprop", "ratingValue").text());
            String userName = rev.getElementsByAttributeValue("itemprop", "author").text();
            String advantages = rev.getElementsByAttributeValue("itemprop", "pro").text();
            String disadvantages = rev.getElementsByAttributeValue("itemprop", "contra").text();
            String comment = rev.getElementsByAttributeValue("itemprop", "reviewBody").text();
            review review_ = new review(rating, userName, advantages, disadvantages, comment);
            list.add(review_);
        }
        return list;
    }
}
