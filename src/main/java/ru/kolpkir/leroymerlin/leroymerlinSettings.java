package ru.kolpkir.leroymerlin;

import ru.kolpkir.ParseLib.ParserSettings;

public class leroymerlinSettings extends ParserSettings {
    public leroymerlinSettings(int start, int end, String category) {
        startPoint = start;
        endPoint = end;
        BASE_URL = "https://leroymerlin.ru/catalogue/" + category;
        PREFIX = "/?page={CurrentId}";
    }
}
