package ru.kolpkir.nanegative;

import ru.kolpkir.ParseLib.ParserSettings;

public class nanegativeReviewsSettings extends ParserSettings {
    public nanegativeReviewsSettings(String baseUrl, int start, int end) {
        startPoint = start;
        endPoint = end;
        BASE_URL = baseUrl;
        PREFIX = "?page={CurrentId}";
    }
}
