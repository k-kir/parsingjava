package ru.kolpkir.leroymerlin;

import ru.kolpkir.ParseLib.ParserWorker;
import ru.kolpkir.leroymerlin.model.product;

import java.util.ArrayList;

public class newDataLeroymerlin implements ParserWorker.OnNewDataHandler<ArrayList<product>> {
    ArrayList<product> products;

    public void OnNewData(Object sender, ArrayList<product> args) {
        products.addAll(args);
    }

    public newDataLeroymerlin(ArrayList<product> products) {
        this.products = products;
    }
}
