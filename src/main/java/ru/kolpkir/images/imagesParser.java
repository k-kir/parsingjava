package ru.kolpkir.images;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;

import java.util.ArrayList;

public class imagesParser implements Parser<ArrayList<String>> {
    int count;

    public ArrayList<String> Parse(Document document) {
        ArrayList<String> list = new ArrayList<String>();
        Elements images = document.getElementsByClass("serp-item__link");
        int i = 0;
        for (var image : images) {
            if (i > count)
                break;
            String link = image.getElementsByClass("serp-item__thumb").get(0).attr("src");
            list.add(link);
            i++;
        }
        return list;
    }

    public imagesParser(int count) {
        this.count = count;
    }
}
