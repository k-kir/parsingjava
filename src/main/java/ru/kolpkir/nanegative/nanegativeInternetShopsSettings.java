package ru.kolpkir.nanegative;

import ru.kolpkir.ParseLib.ParserSettings;

public class nanegativeInternetShopsSettings extends ParserSettings {
    public nanegativeInternetShopsSettings(int start, int end) {
        startPoint = start;
        endPoint = end;
        BASE_URL = "https://nanegative.ru/internet-magaziny";
        PREFIX = "?page={CurrentId}";
    }
}
