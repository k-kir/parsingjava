package ru.kolpkir.nanegative.model;

public class review {
    byte rating;
    String userName;
    String advantages;
    String disadvantages;
    String comment;

    public review(byte rating, String userName, String advantages, String disadvantages, String comment) {
        this.rating = rating;
        this.userName = userName;
        this.advantages = advantages;
        this.disadvantages = disadvantages;
        this.comment = comment;
    }

    public byte getRating() {
        return rating;
    }

    public String getUserName() {
        return userName;
    }

    public String getAdvantages() {
        return advantages;
    }

    public String getDisadvantages() {
        return disadvantages;
    }

    public String getComment() {
        return comment;
    }
}
