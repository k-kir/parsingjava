package ru.kolpkir.leroymerlin;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;
import ru.kolpkir.leroymerlin.model.*;

import java.io.IOException;
import java.util.ArrayList;

public class leroymerlinParser implements Parser<ArrayList<product>> {
    public ArrayList<product> Parse(Document document) {
        ArrayList<product> list = new ArrayList<product>();
        Elements products = document.getElementsByAttribute("data-qa-product");
        for (var prod : products) {
            String name = prod.getElementsByAttributeValue("data-qa", "product-name").text();
            String link = "https://leroymerlin.ru" + prod.getElementsByAttributeValue("data-qa", "product-name").attr("href");
            String price = prod.getElementsByAttributeValue("data-qa", "product-primary-price").text();
            product prod_ = new product(price, name, link);
            try {
                prod_.reviews = ParseProd(Jsoup.connect(link).get());
            } catch (IOException ex) {
                System.out.println("Error: " + ex.getMessage());
            }
            list.add(prod_);
        }
        return list;
    }

    private ArrayList<review> ParseProd(Document document) {
        ArrayList<review> list = new ArrayList<review>();
        Elements reviews = document.getElementsByTag("uc-prp-review-card");
        for (var rev : reviews) {
            double rating = Double.parseDouble(rev.getElementsByAttributeValue("itemprop", "reviewRating").get(0).attr("value"));
            String author = rev.getElementsByAttributeValue("itemprop", "name").get(0).text();
            String datePublished = rev.getElementsByAttributeValue("itemprop", "datePublished").get(0).text();
            String comment = rev.getElementsByAttributeValue("itemprop", "description").get(0).text() + rev.getElementsByClass("term-group").text();
            review review_ = new review(rating, author, datePublished, comment);
            list.add(review_);
        }
        return list;
    }
}
