package ru.kolpkir.nanegative;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.kolpkir.ParseLib.Parser;
import ru.kolpkir.nanegative.model.internetShop;

import java.util.ArrayList;

public class nanegativeInternetShopsParser implements Parser<ArrayList<internetShop>> {
    public ArrayList<internetShop> Parse(Document document) {
        ArrayList<internetShop> list = new ArrayList<internetShop>();
        Elements shops = document.getElementsByClass("find-list-box");
        for (var shop : shops) {
            String name = shop.getElementsByClass("ss").get(0).text().substring(9);
            String link = "https://nanegative.ru" + shop.getElementsByClass("ss").get(0).attr("href");
            String avgRat = shop.getElementsByClass("sro").get(0).text();
            byte averageRating = Byte.parseByte(avgRat.substring(avgRat.length() - 1));
            int countOfReviews = Integer.parseInt(shop.getElementsByClass("num").get(0).text());
            internetShop shop_ = new internetShop(name, link, averageRating, countOfReviews);
            list.add(shop_);
        }
        return list;
    }
}
