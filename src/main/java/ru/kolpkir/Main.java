package ru.kolpkir;

import org.jsoup.Jsoup;
import ru.kolpkir.ParseLib.*;
import ru.kolpkir.images.*;
import ru.kolpkir.leroymerlin.*;
import ru.kolpkir.leroymerlin.model.*;
import ru.kolpkir.nanegative.model.*;
import ru.kolpkir.nanegative.*;
import ru.kolpkir.news.*;
import ru.kolpkir.news.model.news;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static WriteReviews writer;
    static WriteReviewsLeroy writerLeroy;
    static WriteNews writerNews;

    public static void main(String[] args) {
        byte action;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Выберите пункт:\n1: Сбор отзывов об интернет-магазинах с сайта nanegative.ru\n2: Сбор отзывов с сайта leroymerlin\n3: Скачивание изображений\n4: Новости с сайта ТАСС\n5: Новости с сайта BBC\n0: Выход");
            action = scanner.nextByte();
            switch (action) {
                case 1 -> nanegative();
                case 2 -> leroymerlin();
                case 3 -> images();
                case 4 -> tass();
                case 5 -> bbc();
                case 0 -> System.out.println("До свидания");
                default -> System.out.println("Неверная команда.");
            }
        } while (action != 0);
    }

    private static void nanegative() {
        Scanner scanner0 = new Scanner(System.in);
        System.out.println("Введите путь до папки сохранения отзывов: ");
        String reviewsFolderPath = scanner0.nextLine();
        if (!Files.exists(Path.of(reviewsFolderPath)) || !Files.isDirectory(Path.of(reviewsFolderPath))) {
            System.out.println("Пути не существует или это не директория.");
            return;
        }
        ArrayList<internetShop> shops = new ArrayList<>();
        ParserWorker<ArrayList<internetShop>> parserInternetShops = new ParserWorker<>(new nanegativeInternetShopsParser());
        //Pair<Integer, Integer> pages = getPages();
        parserInternetShops.setParserSettings(new nanegativeInternetShopsSettings(1, 10));
        //parserInternetShops.setParserSettings(new nanegativeInternetShopsSettings(pages.first, pages.second)); //1,10
        parserInternetShops.onNewDataList.add(new newDataNaNegativeInternetShops(shops));
        try {
            parserInternetShops.Start();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        parserInternetShops.Abort();
        for (var shop : shops) {
            ParserWorker<ArrayList<ru.kolpkir.nanegative.model.review>> parserReviews = new ParserWorker<>(new nanegativeReviewsParser());
            int countOfPages = shop.getCountOfReviews() % 30 == 0 ? shop.getCountOfReviews() / 30 : shop.getCountOfReviews() / 30 + 1;
            System.out.println("Записываются отзывы о магазине " + shop.getName());
            //Pair<Integer, Integer> pages1 = getPages();
            //if (pages1.second > countOfPages)
            //    pages1.second = countOfPages;
            //if (pages1.first <= pages1.second) {
            parserReviews.setParserSettings(new nanegativeReviewsSettings(shop.getLink(), 1, countOfPages));
            //parserReviews.setParserSettings(new nanegativeReviewsSettings(shop.getLink(), pages1.first, pages1.second)); //1, countOfPages
            parserReviews.onNewDataList.add(new newDataNaNegativeReviews(shop.reviews));
            try {
                parserReviews.Start();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            parserReviews.Abort();
            writer = new WriteReviews(reviewsFolderPath + "\\" + cleanStr(shop.getName()) + ".txt", shop.reviews);
            Thread writeThread = new Thread(writer);
            writeThread.start();
            //}
        }
    }

    private static void leroymerlin() {
        Scanner scanner0 = new Scanner(System.in);
        System.out.println("Введите путь до папки сохранения отзывов: ");
        String reviewsFolderPath = scanner0.nextLine();
        if (!Files.exists(Path.of(reviewsFolderPath)) || !Files.isDirectory(Path.of(reviewsFolderPath))) {
            System.out.println("Пути не существует или это не директория.");
            return;
        }
        ArrayList<product> products = new ArrayList<>();
        ParserWorker<ArrayList<product>> parserProducts = new ParserWorker<>(new leroymerlinParser());
        System.out.println("Введите категорию: ");
        String category = scanner0.nextLine();
        parserProducts.setParserSettings(new leroymerlinSettings(1, 10, category));
        try {
            if (Jsoup.connect(parserProducts.getParserSettings().getBaseUrl()).get().title().contains("404")) {
                System.out.println("Такой категории нет.");
                return;
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        int endPage = parserProducts.getParserSettings().getEndPoint();
        try {
            endPage = Integer.parseInt(Jsoup.connect(parserProducts.getParserSettings().getBaseUrl() + parserProducts.getParserSettings().getPrefix().replace("{CurrentId}", "999")).get().getElementsByAttributeValue("data-qa-pagination-active", "true").attr("data-qa-pagination-item"));
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        Pair<Integer, Integer> pages = getPages();
        if (pages.second > endPage)
            pages.second = endPage;
        if (pages.first <= pages.second) {
            parserProducts.setParserSettings(new leroymerlinSettings(pages.first, pages.second, category));
            parserProducts.onNewDataList.add(new newDataLeroymerlin(products));
            try {
                parserProducts.Start();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            parserProducts.Abort();
            for (var prod : products) {
                writerLeroy = new WriteReviewsLeroy(reviewsFolderPath + "\\" + cleanStr(prod.getName()) + ".txt", prod.reviews);
                Thread writeThread = new Thread(writerLeroy);
                writeThread.start();
            }
        }
    }

    public static void images() {
        Scanner scanner0 = new Scanner(System.in);
        System.out.println("Введите путь до папки сохранения картинок: ");
        String imagesFolderPath = scanner0.nextLine();
        if (!Files.exists(Path.of(imagesFolderPath)) || !Files.isDirectory(Path.of(imagesFolderPath))) {
            System.out.println("Пути не существует или это не директория.");
            return;
        }
        ArrayList<String> links = new ArrayList<>();
        System.out.println("Введите запрос: ");
        String query = scanner0.nextLine();
        System.out.println("Введите количество изображений: ");
        int count = scanner0.nextInt();
        ParserWorker<ArrayList<String>> parserLinks = new ParserWorker<>(new imagesParser(count));
        parserLinks.setParserSettings(new imagesSettings(query));
        parserLinks.onNewDataList.add(new newDataImages(links));
        try {
            parserLinks.Start();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        parserLinks.Abort();
        for (var link : links) {
            try {
                if (link.startsWith("//"))
                    link = "http:" + link;
                URL image = new URL(link);
                InputStream in = image.openStream();
                Files.copy(in, Paths.get(imagesFolderPath + "\\" + query + "_" + java.util.UUID.randomUUID() + ".webp"));
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        }
    }

    public static void tass() {
        Scanner scanner0 = new Scanner(System.in);
        System.out.println("Введите путь до папки сохранения новостей: ");
        String newsFolderPath = scanner0.nextLine();
        if (!Files.exists(Path.of(newsFolderPath)) || !Files.isDirectory(Path.of(newsFolderPath))) {
            System.out.println("Пути не существует или это не директория.");
            return;
        }
        ArrayList<news> newss = new ArrayList<>();
        System.out.println("Введите количество новостей: ");
        int count = scanner0.nextInt();
        ParserWorker<ArrayList<news>> parserTass = new ParserWorker<>(new tassParser(count));
        parserTass.setParserSettings(new tassSettings());
        parserTass.onNewDataList.add(new newDataTass(newss));
        try {
            parserTass.Start();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        parserTass.Abort();
        for (var ns : newss) {
            writerNews = new WriteNews(newsFolderPath + "\\" + cleanStr(ns.heading) + ".txt", ns);
            Thread writeThread = new Thread(writerNews);
            writeThread.start();
        }

    }

    public static void bbc() {
        Scanner scanner0 = new Scanner(System.in);
        System.out.println("Введите путь до папки сохранения новостей: ");
        String newsFolderPath = scanner0.nextLine();
        if (!Files.exists(Path.of(newsFolderPath)) || !Files.isDirectory(Path.of(newsFolderPath))) {
            System.out.println("Пути не существует или это не директория.");
            return;
        }
        ArrayList<news> newss = new ArrayList<>();
        System.out.println("Введите количество новостей: ");
        int count = scanner0.nextInt();
        ParserWorker<ArrayList<news>> parserBbc = new ParserWorker<>(new bbcParser(count));
        parserBbc.setParserSettings(new bbcSettings());
        parserBbc.onNewDataList.add(new newDataBbc(newss));
        try {
            parserBbc.Start();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        parserBbc.Abort();
        for (var ns : newss) {
            writerNews = new WriteNews(newsFolderPath + "\\" + cleanStr(ns.heading) + ".txt", ns);
            Thread writeThread = new Thread(writerNews);
            writeThread.start();
        }

    }

    public static String cleanStr(String string) {
        ArrayList<Character> banned = new ArrayList<Character>(Arrays.asList('\\', '/', ':', '*', '?', '\"', '<', '>', '|'));
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            if (banned.contains(string.charAt(i)))
                res.append("_");
            else
                res.append(string.charAt(i));
        }
        return res.toString();
    }

    public static Pair<Integer, Integer> getPages() {
        int begin, end;
        Scanner scanner0 = new Scanner(System.in);
        do {
            System.out.println("Введите начальную страницу: ");
            begin = scanner0.nextInt();
            if (begin < 1) {
                System.out.println("Значение должно быть натуральным.");
            }
        } while (begin < 1);

        do {
            System.out.println("Введите конечную страницу: ");
            end = scanner0.nextInt();
            if (end < begin) {
                System.out.println("Значение должно быть не меньше значения начальной страницы.");
            }
        } while (end < begin);
        return new Pair<>(begin, end);
    }
}

class Pair<T, K> {
    public T first;
    public K second;

    public Pair(T first, K second) {
        this.first = first;
        this.second = second;
    }
}

class WriteReviews implements Runnable {
    String fileName;
    ArrayList<ru.kolpkir.nanegative.model.review> reviews;

    public WriteReviews(String fileName, ArrayList<ru.kolpkir.nanegative.model.review> reviews) {
        this.fileName = fileName;
        this.reviews = reviews;
    }

    public void run() {
        for (var rev : reviews) {
            String revS = "Рейтинг: " + rev.getRating() + "\n";
            revS += "Пользователь: " + rev.getUserName() + "\n";
            revS += "Плюсы: " + rev.getAdvantages() + "\n";
            revS += "Минусы: " + rev.getDisadvantages() + "\n";
            revS += "Отзыв: " + rev.getComment() + "\n";
            revS += "___________________________________________________\n";
            try (FileWriter writer = new FileWriter(fileName, true)) {
                writer.write(revS);
                writer.flush();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

class WriteReviewsLeroy implements Runnable {
    String fileName;
    ArrayList<ru.kolpkir.leroymerlin.model.review> reviews;

    public WriteReviewsLeroy(String fileName, ArrayList<ru.kolpkir.leroymerlin.model.review> reviews) {
        this.fileName = fileName;
        this.reviews = reviews;
    }

    public void run() {
        for (var rev : reviews) {
            String revS = "Рейтинг: " + rev.getRating() + "\n";
            revS += "Автор: " + rev.getAuthor() + "\n";
            revS += "Дата публикации: " + rev.getDatePublished() + "\n";
            revS += "Отзыв: " + rev.getComment() + "\n";
            revS += "___________________________________________________\n";
            try (FileWriter writer = new FileWriter(fileName, true)) {
                writer.write(revS);
                writer.flush();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

class WriteNews implements Runnable {
    String fileName;
    news ns;

    public WriteNews(String fileName, news ns) {
        this.fileName = fileName;
        this.ns = ns;
    }

    public void run() {
        String newStr = ns.heading + "\n";
        newStr += "___________________________________________________\n";
        newStr += ns.text + "\n";
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(newStr);
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}